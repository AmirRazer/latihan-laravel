<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DashboardController@index');

Route::get('/form', 'FormController@bio');


Route::post('/welcome','FormController@kirim');

Route::get('/data-table', function(){
    return view('table.data-table');
});

//crud kategori

route::get('/cast/create', 'CastController@create');
route::post('/cast', 'CastController@store');
route::get('/cast', 'CastCOntroller@index');
route::get('/cast/{cast_id}', 'CastCOntroller@show');
route::get('/cast/{cast_id}/edit', 'CastController@edit');
route::put('/cast/{cast_id}','CastController@update');
route::delete('/cast/{cast_id}','CastController@destroy');