@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')

<a href="/cast/create" class="btn btn-success mb-3 " >Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">NOMOR</th>
        <th scope="col">NAMA</th>
        <th scope="col">UMUR</th>
        <th scope="col">BIO</th>
        <th scope="col">ACTION</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
      <tr>
      <td>{{$key + 1}}</td>
      <td>{{$item->nama}}</td>
      <td>{{$item->umur}}</td>
      <td>{{$item->bio}}</td>
      <td>
      <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm " >Detail</a>
      <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm " >Edit</a>
      <form action="/cast/{{$itm->id}}" method="POST">
        @method('delete')
        @csrf
        <input type="submit" class="btn btn-danger btn-sm " value="Delete">
      </form>

       
      </td>
    </tr>  
      @empty
      <tr>
          <td>Data Masih Kosong</td>
      </tr>
      @endforelse
    </tbody>
  </table>

  @endsection